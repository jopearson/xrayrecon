from pathlib import Path

import cv2 as cv
import numpy as np
import pyqtgraph.opengl as gl
from pyqtgraph.Qt import QtGui, QtWidgets
from qtextras import EasyWidget


class PCBViewer3D(gl.GLViewWidget):
    downsampFactor = 1

    def __init__(self, data=None):
        super().__init__()
        self.opts["distance"] = 200
        self.setWindowTitle("pyqtgraph example: GLVolumeItem")
        g = self.gridItem = gl.GLGridItem()
        self.addItem(g)
        self.curVolume = None
        ax = gl.GLAxisItem()
        self.addItem(ax)
        self.volume = gl.GLVolumeItem(np.zeros((1, 1), dtype="uint8"))
        self.addItem(self.volume)

        if data is not None:
            self.setDataCube(data)

    def setDataCube(
        self, data, highlightIdxs=None, downsampleFactor=None, transparencyFactor=3
    ):
        if downsampleFactor is None:
            downsampleFactor = self.downsampFactor
        self.downsampFactor = downsampleFactor
        if downsampleFactor > 1:
            data, highlightIdxs = self.downsampleData(
                data, highlightIdxs, downsampleFactor
            )

        self.gridItem.scale(*np.array(data.shape[:2][::-1]), data.shape[-1])
        d2 = np.empty(data.shape + (4,), dtype=np.ubyte)
        d2[..., 0:3] = data[..., None]
        d2[..., 3] = ((data * 0.6) / 255) ** transparencyFactor * 255
        if highlightIdxs is not None:
            d2[:, :, highlightIdxs, 0:2] = 0
            d2[:, :, highlightIdxs, 3] = d2[:, :, highlightIdxs, 3] * 1.2

        # RGB axis alignment items
        d2[:, 0, 0] = [255, 0, 0, 100]
        d2[0, :, 0] = [0, 255, 0, 100]
        d2[0, 0, :] = [0, 0, 255, 100]

        self.volume.setData(d2)

    def resetZoom(self):
        """Broken! Do not use."""
        self.setCameraPosition((0, 0), self.volume.data.shape[2] + 10, -90, 0)

    def downsampleData(self, data, highlightIdxs, factor=None):
        if factor is None:
            factor = self.downsampFactor
        self.downsampFactor = factor
        newSize = np.array(data.shape)
        newSize //= [factor] * 3
        outCube = np.zeros(newSize, dtype="uint8")
        for ii, resizeIdx in enumerate(
            np.linspace(0, data.shape[-1] - 1, newSize[-1], dtype=int)
        ):
            outCube[..., ii] = cv.resize(data[..., resizeIdx], outCube.shape[:2][::-1])
        # np.save('/home/UFAD/njessurun/Documents/resized.npy', outCube)
        if highlightIdxs is not None:
            highlightIdxs = np.unique((highlightIdxs // factor))
            highlightIdxs = highlightIdxs[highlightIdxs < outCube.shape[2]]
        return outCube, highlightIdxs


if __name__ == "__main__":
    inpath = Path("C:/Users/njessurun/downloads/data_iotech.npy")
    from xrayrecon.gui.layermodel import LayerModel

    data = np.load(inpath)
    data = LayerModel.forceDepthToLastAx(data)
    orig = data
    lbl = QtWidgets.QLabel("hello")
    app = QtGui.QApplication([])
    viewer = PCBViewer3D()
    viewer.setDataCube(data, downsampleFactor=5)
    w = EasyWidget.buildMainWindow([lbl, viewer], useSplitter=True)
    w.show()
